angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('bluume1', {
    url: '/page5',
    templateUrl: 'templates/bluume1.html',
    controller: 'bluume1Ctrl'
  })

  .state('bluume2', {
    url: '/page9',
    templateUrl: 'templates/bluume2.html',
    controller: 'bluume2Ctrl'
  })

  .state('bluume3', {
    url: '/page10',
    templateUrl: 'templates/bluume3.html',
    controller: 'bluume3Ctrl'
  })

  .state('bluume4', {
    url: '/page11',
    templateUrl: 'templates/bluume4.html',
    controller: 'bluume4Ctrl'
  })

  .state('bluume5', {
    url: '/page13',
    templateUrl: 'templates/bluume5.html',
    controller: 'bluume5Ctrl'
  })

  .state('bluume6', {
    url: '/page14',
    templateUrl: 'templates/bluume6.html',
    controller: 'bluume6Ctrl'
  })

  .state('bluume7', {
    url: '/page15',
    templateUrl: 'templates/bluume7.html',
    controller: 'bluume7Ctrl'
  })

  .state('bluume8', {
    url: '/page17',
    templateUrl: 'templates/bluume8.html',
    controller: 'bluume8Ctrl'
  })

$urlRouterProvider.otherwise('/page5')


});